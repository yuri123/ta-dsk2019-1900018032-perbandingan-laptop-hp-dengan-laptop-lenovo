;nama : yuri mahdi p
;nim  : 1900018032
.MODEL SMALL 					;.model SMALL, fungsinya untuk memberitahukan kepada assembler bentuk memory yang digunakan oleh program kita.
.CODE						;.code, digunakan untuk memberitahukan kepada assembler bahwa kita akan mulai menggunakan Code Segment-nya disini.
ORG 100h					;ORG 100h, digunakan untuk memberitahukan assembler agar pada saat program dijalankan mulai pada offset ke 100h.
tdata:jmp proses				;tdata: dimana kita bisa deklarasikan variabel-variabel ataupun data-data yang digunakan dalam program kita, lalu melompat ke proses
	lusername db 13,10,'username : $'	;Perintah JMP(JUMP) ini digunakan untuk melompat menuju tempat yang ditunjukkan oleh perintah JUMP.
	lpassword db 13,10,'password : $'
	lditerima db 13,10,'diterima$'
	lditolak db 13,10,'ditolak$
						;menyimpan memori (var) untuk menerima inputan keyboard.
        vusername db 23,?,23 dup(?)		;berguna untuk mengulang variabel buffer sebanyak 1 kali yang besarnya hanya 1 byte.
	vpassword db 23,?,23 dup(?)
proses: 
	mov ah,09h				;mov ah,09 untuk mencetak karakter	
	lea dx,lusername			;lea dx,lusername untuk mendapatkan alamat efektif atau offset dari lusername.
	int 21h					;int 21h yang berguna untuk menghentikan program yang dijalankan

	mov ah,0ah
	lea dx,vusername
	int 21h

	mov ah,09h
	lea dx,lpassword
	int 21h

	mov ah,0ah
	lea dx,vpassword
	int 21h

	lea si,vusername			;LEA adalah perintah untuk mendapatkan alamat dari sebuah variabel.
	lea di,vpassword

	cld					;CLD (Clear Direction Flag) berfungsi untuk menaikkan nilai SI dan DI (arah proses menaik)
	mov cx,23				;Banyaknya karakter yang ingin dicetak 
	rep cmpsb				;Kalimat di atas menggambarkan jika masukkan passwordnya salah sehingga ketika di bandingkan menggunakan perintah REP CMPSB.
	jne gagal				;JNE ( Jump If Not Equal ) yaitu Lompat, jika operand1 tidak sama dengan operand2

	mov ah,09h
	lea dx,lditolak				;lea dx,lusername untuk mendapatkan alamat efektif atau offset dari lditolak.
	int 21h
	jmp proses				;jmp proses digunakan untuk mengulangi proses/melompat (jump) kembali ke proses awal
gagal:
	mov ah,09h
	lea dx,lditerima			;lea dx,lusername untuk mendapatkan alamat efektif atau offset dari lditerima.
	int 21h
	jmp exit				;jmp exit digunakan untuk mengakhiri proses/melompat (jump) mengakhiri proses
exit:
	int 20h
end tdata					;end tdata merupakan penutupan dari label tdata yang telah kita buat.